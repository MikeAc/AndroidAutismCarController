package networkManager

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.NetworkRequest
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import java.io.*
import java.lang.Exception
import java.net.*
import java.security.KeyStore
import java.security.spec.ECGenParameterSpec
import java.util.*
import kotlin.concurrent.thread
import kotlin.concurrent.timer

class networkManager(handler: Handler, val senderPort: Int, val discoveryPort:Int) {

    companion object Factory {
        val NOT_CONNECTED_ERROR=33
        val CONNECTION_SUCCESFUL=92
        val LOST_CONNECTION=32
        val SCANNING=34
        val PROCESSING_FILE=35
        val REQUEST_REJECTED=36
        val FILE_TRANSFER_FAILURE=37
        val FILE_TRANSFER_PROCESS=38
        val FILE_TRANSFER_SUCCESSFUL=309
    }
    private val udpsock=DatagramSocket()
    private val udpsocklistener=DatagramSocket()
    private var senderthread:Thread?=null
    private var receivethread:Thread?=null

    private var okflag=true
    var currIpServer: InetAddress? =null
    private  var broadcastcont=0
    private val udpBroadcastsock=DatagramSocket()
    private var udpbroadcastsockreceiver: DatagramSocket? =null
    private  lateinit var filesocket:Socket
    private lateinit var cmdtcpSock:Socket
    private  var fileThread:Thread?=null
    private lateinit var filetype:String
    private lateinit var  currFilenm:String

    private lateinit var timing:Timer

    val currrhandler=handler
    private lateinit var currFileInput:FileInputStream
    private var currFilesize:Long=0

    init {
        udpBroadcastsock.setBroadcast(true)
        try {


            udpbroadcastsockreceiver = DatagramSocket(discoveryPort, InetAddress.getByName("0.0.0.0"))
        }catch (e:Exception)
        {
            Log.wtf("InitsockFailure", "" + e.message)
        }
        udpbroadcastsockreceiver?.broadcast = true
        udpbroadcastsockreceiver?.soTimeout = 5000
        //udpsocklistener.soTimeout=1000
        receivethread = thread() { receive() }


    }

    private fun receive() {

         loop@ while (true) {
             try {



                 var buffreceiver = ByteArray(100)
                 var recPacket = DatagramPacket(buffreceiver, 100)

                 udpbroadcastsockreceiver?.receive(recPacket)
                 var msg= String(buffreceiver, Charsets.UTF_8).slice(IntRange(0,recPacket.length-1))

                    var args=msg.split(":")
                 Log.d("msgBroacastList",args.toString())
                    if(args.size>=2 && args[0].equals("ok")) {

                        when
                        {
                            args[1].equals("ip_request_response") ->{
                        Log.d(
                            "BroadcastReceive",
                            "From: " + recPacket.address.hostAddress +"size:"+ recPacket.length  +"  \n" +msg)

                            okflag=true
                                timing.cancel()
                                currIpServer = recPacket?.address
                                socketcarConnection(recPacket?.address,senderPort)

                                break@loop
                            }
                        }
                    }



             } catch (e: Exception) {

                 Log.wtf("broadcast Connection Failure Rec", "" + e.message)
                 break@loop

             }


         }
         Log.d("broadcastReceive", "ending")
     }
    private    fun getBroadcastIp() : InetAddress? {
             var interfaces = NetworkInterface.getNetworkInterfaces()
             var broadcastAddres: InetAddress? = null

             for (netItem in interfaces.toList()) {
                 if (netItem.name.startsWith("wlan")) {
                     Log.d("net: ", netItem.displayName)
                     for (itemAdrr in netItem.interfaceAddresses.toList()) {

                         itemAdrr.let { broadcastAddres = itemAdrr.broadcast; }

                     }
                 }
             }

             return broadcastAddres
         }

    private  fun  getServerIp() {

            if(senderthread?.isAlive()==true)
                return
        broadcastcont++
        if(broadcastcont>5)
        {
            okflag=true
            broadcastcont=0
            timing.cancel()
            var msg=Message()
            msg.arg1=networkManager.NOT_CONNECTED_ERROR
            currrhandler.sendMessage(msg)
            return

        }

        var REQUESTIP_MESSAGE="cmd:get_ip".toByteArray()
        var broadcastAddres=getBroadcastIp()

       senderthread= thread() {

               try {
                   var packet =
                       DatagramPacket(REQUESTIP_MESSAGE, REQUESTIP_MESSAGE.size, broadcastAddres, discoveryPort)
                   udpBroadcastsock.send(packet)

                   Log.d("broadcast", broadcastAddres?.hostAddress)

                   //udpBroadcastsock.close()
               } catch (e: Exception) {
                   Log.wtf("broadcast Connection Failure Send", "" + e.message)
               }
           }



        if(receivethread?.isAlive()==false)
            receivethread= thread { receive() }

        }
         fun starScanning(){
            if(!okflag)
                return
             var msg=Message()
             msg.arg1=networkManager.SCANNING
             currrhandler.sendMessage(msg)
            okflag=false

            timing=timer("timing", period = 1000){

                getServerIp()
            }
        }

    fun socketcarConnection(address: InetAddress, port:Int){
        try {
            udpsock.disconnect()

            udpsock.connect(address,port)
            Log.d("ConnectMainUdp", "ip= "+address.toString()+" Port: "+port.toString()+"\nConnected: "+udpsock.isConnected.toString())
            var msg=Message()
            udpsocklistener.disconnect()
            udpsocklistener.connect(address,port)
            msg.arg1=networkManager.CONNECTION_SUCCESFUL
            msg.obj=currIpServer?.hostAddress
            currrhandler.sendMessage(msg)
        }catch (e:Exception)
        {
            Log.wtf("Connection Failure", "" + e.message)
        }

    }

    fun sendPackage(msg : ByteArray )
    {
        if(!udpsock.isConnected) {
            Log.wtf("Send failure", "Not Connected")
            return
        }
        thread() {

            try {
                var packet = DatagramPacket(msg, msg.size)
                udpsock.send(packet)
            } catch (e: Exception) {
                Log.wtf("Send failure", "" + e.message+"connected? ")
                var msg=Message()
                msg.arg1=networkManager.LOST_CONNECTION
                currrhandler.sendMessage(msg)
                udpsock.disconnect()

            }
        }


    }
    fun closeAll(){

        try {
            udpBroadcastsock.disconnect()
            udpBroadcastsock.close()
            udpsock.disconnect()
            udpsock.close()
            timing.cancel()
        }catch (e:Exception)
        {
            Log.wtf("NetworkManager", "" + e.message)

        }
       // senderthread?.stop()
       // receivethread?.stop()
    }
    fun setbuffer(file: FileInputStream , fileSize:Long ,type:String,fileNm:String)
    {        this.currFilesize=fileSize


        if (fileThread!=null)
        {
        if (fileThread!!.isAlive)
        {
            var msg=Message()
            msg.arg1=networkManager.PROCESSING_FILE
            currrhandler.sendMessage(msg)

            return
        }
        }
        currFileInput=file
        filetype=type
        currFilenm=fileNm
        fileThread= thread { fileprocessing() }

    }


    private fun fileprocessing()
    {
        var socket:Socket?=null

        var out:PrintWriter?=null


        var inputs:BufferedReader?=null
        Log.d("tcpprocessor", "in")

        try {
                socket=Socket(currIpServer,discoveryPort)

                var mrun=true
                 out=PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())),true)
                inputs= BufferedReader(InputStreamReader(socket.getInputStream()))
            out?.also {
                if (!it.checkError()) {
                    it.write("file:" + filetype + ":" + currFilenm + ":" + currFilesize)
                    it.flush()

                    var message = inputs?.readText()
                    while (mrun) {

                    message.also {
                        if(message.equals("ok:allowed"))
                        {
                            if(sendFile())
                            {  var msg=Message()
                                msg.arg1=networkManager.FILE_TRANSFER_SUCCESSFUL
                                currrhandler.sendMessage(msg)
                                }
                        }
                        else
                        {
                            var msg=Message()
                            msg.arg1=networkManager.REQUEST_REJECTED
                            currrhandler.sendMessage(msg)
                        }

                        mrun = false
                    }
                }

            }
            }

            } catch (e: Exception) {
            var msg=Message()
            msg.arg1=networkManager.LOST_CONNECTION
            currrhandler.sendMessage(msg)
                Log.wtf("broadcast Connection Failure Rec", "" + e.message)


            }
        finally {

        out?.flush()
            out?.close()
            inputs?.close()
            socket?.close()
        }



    }

    private fun sendFile():Boolean
    {
        var filesock:Socket?=null
        var outfile:OutputStream?=null
        var filelength=currFilesize
        var current:Long=0
        var bytereadbuffersize=0

        var byteread=0
        Log.d("Filsender"," in ")
        try {


            filesock = Socket(currIpServer, senderPort)
            outfile= DataOutputStream(filesock?.getOutputStream())

            var binpfile=BufferedInputStream(currFileInput)
            val pct=100/currFilesize.toDouble()
            while(current!=filelength)
            {

                var buffsize=1000
                if(filelength-current>=buffsize)
                    current+=buffsize
                else {
                    buffsize = (filelength - current).toInt()
                    current=filelength
                }
                var buffer=ByteArray(buffsize)
                bytereadbuffersize=binpfile.read(buffer,0,buffsize)
                byteread+=bytereadbuffersize

                //Log.d("FileTransfer","Bytes from File "+ byteread)
               // Log.d("FileTransfer","pasofileread")
                outfile.write(buffer)
              //  Log.d("FileTransfer","paso write")
                val progress=(byteread*pct).toInt()
                if(progress%10==0) {
                    var msg = Message()
                    msg.arg1 = networkManager.FILE_TRANSFER_PROCESS
                    msg.arg2=progress
                    currrhandler.sendMessage(msg)
                }





            }

        }
        catch (e:Exception)
        {
            Log.wtf("TcpSendFile",e.message)
            var msg=Message()
            msg.arg1=networkManager.FILE_TRANSFER_FAILURE
            currrhandler.sendMessage(msg)

        }
        finally {
            filesock?.close()
            outfile?.flush()
            outfile?.close()

        }
        return true
    }

    }
