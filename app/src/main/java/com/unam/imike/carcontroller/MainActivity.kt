package com.unam.imike.carcontroller

import android.app.*
import android.content.ContentResolver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.*
import android.os.Message
import android.provider.MediaStore
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.ActionMenuItem
import android.text.style.TtsSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.VideoView
import io.github.controlwear.virtual.joystick.android.JoystickView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import networkManager.networkManager
import java.io.*
import java.lang.Exception
import java.lang.ref.WeakReference
import java.net.InetAddress
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest
import javax.xml.transform.Result

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var  netManager:networkManager?=null
    var aDialogNotConnected: AlertDialog.Builder?=null

   private  lateinit var handler:HandlerMesg

   //permissions constants
    private val STORAGE_REQUEST=1
    private val CAMERA_PHOTO_REQUEST=2
    private val CAMERA_VIDEO_REQUEST=3
    private val RECORD_REQUEST=6

    ///
    private val IMAGE_REQUEST=1
    private val VIDEO_RQUEST=4
    lateinit var  notificationManager: NotificationManager
    lateinit var  filenotificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
   lateinit var builder:Notification.Builder
    lateinit var builder_filestatusnotification: Notification.Builder
    lateinit var  barbuilder:Notification.Builder
    private val channelId="com.unam.imike.carcontroller"
    private val description="ConnectionNotification"
    val notificationId=309
    private  var currentMediaPath=""
    private var currFileSize:Long=0
     class HandlerMesg(activity: MainActivity):Handler()   {
         var currActivity: WeakReference<MainActivity> = WeakReference(activity)


         override fun handleMessage(msg: Message?) {
             super.handleMessage(msg)
             Log.d("handler", " msg:" + msg?.arg1.toString())

             when (msg?.arg1) {
                 networkManager.LOST_CONNECTION -> {
                    notify(msg,2)
                     currActivity.get()?.aDialogNotConnected?.show()
                 }
                 networkManager.CONNECTION_SUCCESFUL -> {
                    notify(msg,1)
                 }
                 networkManager.NOT_CONNECTED_ERROR ->
                     notify(msg,2)
                 networkManager.SCANNING ->
                     notify(msg,3)
                 networkManager.PROCESSING_FILE->
                     notify(msg,4)
                 networkManager.REQUEST_REJECTED->
                     notify(msg,5)
                 networkManager.FILE_TRANSFER_FAILURE->
                     notify(msg,6)
                 networkManager.FILE_TRANSFER_PROCESS->
                     notify(msg,7)
                 networkManager.FILE_TRANSFER_SUCCESSFUL->
                     notify(msg,7)
             }
         }

             fun notify(msg: Message?, type:Int)
             {
                 when(type)
                 {1->{
                     var addr: String =msg?.obj as String
                     currActivity.get()?.builder?.setContentText(currActivity.get()?.getString(R.string.notify_connected)+addr)?.
                         setLargeIcon(BitmapFactory.decodeResource(currActivity.get()?.resources,R.drawable.ic_connected))

                     currActivity.get()?.notificationManager?.notify(currActivity.get()?.notificationId!!,currActivity.get()?.builder?.build())

                 }
                     2->{
                         currActivity.get()?.builder?.setContentText( currActivity.get()?.getText(R.string.notify_notconnected) )?.
                             setLargeIcon(BitmapFactory.decodeResource(currActivity.get()?.resources,R.drawable.ic_notconnected))

                         currActivity.get()?.notificationManager?.notify(currActivity.get()?.notificationId!!,currActivity.get()?.builder?.build())
                     }
                     3->{
                         currActivity.get()?.builder?.setContentText(currActivity.get()?.getText(R.string.notify_scanning))?.
                             setLargeIcon(BitmapFactory.decodeResource(currActivity.get()?.resources,R.drawable.ic_scanning))

                         currActivity.get()?.notificationManager?.notify(currActivity.get()?.notificationId!!,currActivity.get()?.builder?.build())
                     }
                     4->{
                         currActivity.get()?.builder_filestatusnotification?.setContentText(currActivity.get()?.getText(R.string.processingNotification))

                         currActivity.get()?.filenotificationManager?.notify(currActivity.get()?.notificationId!!+2,currActivity.get()?.builder_filestatusnotification?.build())


                     }
                     5->{
                         currActivity.get()?.builder_filestatusnotification?.setContentText("Ups no se puede iniciar la transferencia verifique la app del carro")

                         currActivity.get()?.filenotificationManager?.notify(currActivity.get()?.notificationId!!+1,currActivity.get()?.builder_filestatusnotification?.build())

                     }
                     6->{
                         currActivity.get()?.builder_filestatusnotification?.setContentText("La transferencia fallo...")

                         currActivity.get()?.filenotificationManager?.notify(currActivity.get()?.notificationId!!+3,currActivity.get()?.builder_filestatusnotification?.build())

                     }
                     7->{


                         if(msg?.arg1==networkManager.FILE_TRANSFER_SUCCESSFUL) {
                             Log.d(" Notify"," CompletadoArchivo")
                             currActivity.get()?.barbuilder?.setContentText("Completed")?.setProgress(0,0,false)
                         }
                         else
                             currActivity.get()?.barbuilder?.setProgress(100, msg?.arg2!!,false)
                         currActivity.get()?.filenotificationManager?.notify(currActivity.get()?.notificationId!!+4,currActivity.get()?.barbuilder?.build())

                     }
                 }

             }


     }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val ctrlButton=findViewById<FloatingActionButton>(R.id.ctrlButton)
        val leftJoystick=findViewById<JoystickView>(R.id.joystickLeft)
        val rightJoystick=findViewById<JoystickView>(R.id.joystickRight)
        createFixedNotification()
        aDialogNotConnected=AlertDialog.Builder(this,android.R.style.Theme_Material_Dialog)
        aDialogNotConnected?.setTitle("Error")?.
            setMessage("No conectado")?.setPositiveButton("ok",
            DialogInterface.OnClickListener(){ dialog: DialogInterface?, which: Int ->


            })
        handler= HandlerMesg(this)
        netManager=networkManager(handler,6551,6552)


        leftJoystick.setOnMoveListener{angle, strength ->
            onMove(leftJoystick,angle,strength)

        }

        rightJoystick.setOnMoveListener{angle, strength ->
            onMove(rightJoystick,angle,strength)
        }
        leftJoystick.setOnFocusChangeListener{v, hasFocus ->
            Snackbar.make(v, "focus", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }


        ctrlButton.setOnClickListener { view ->
            Snackbar.make(view, "Ctrl button", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        netManager?.starScanning()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            R.id.action_refresh-> {
               // Log.d("Actions","Refresh")
                netManager?.starScanning()
                return true
            }


            else -> return super.onOptionsItemSelected(item)

        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera , R.id.nav_camera_video-> {
                // Handle the camera actio

                if(checkSelfPermission(android.Manifest.permission.CAMERA) !=PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED  &&
                    checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=PackageManager.PERMISSION_GRANTED)

                    requestPermissions(arrayOf(android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ),CAMERA_PHOTO_REQUEST)

                    else {
                    var req=if(item.itemId==R.id.nav_camera)CAMERA_PHOTO_REQUEST
                       else CAMERA_VIDEO_REQUEST

                    val intent = if(item.itemId==R.id.nav_camera)Intent(MediaStore.ACTION_IMAGE_CAPTURE) else
                      Intent(MediaStore.ACTION_VIDEO_CAPTURE)

                    if (intent.resolveActivity(packageManager) != null) {
                        val photoFile: File?= try {

                                createFile(req)
                        }catch (ex:IOException)
                        {
                            Log.wtf("FileError",ex.message)
                            null
                        }
                        photoFile?.also {
                            val photoURI:Uri =FileProvider.getUriForFile(this,"com.unam.imike.carcontroller.provider",it)
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI)
                            startActivityForResult(intent, req)
                        }


                    }
                }

            }
            R.id.nav_gallery,R.id.nav_gallery_video, R.id.nav_record -> {
                val checkselfPermission=ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)

                if(checkselfPermission!=PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.RECORD_AUDIO) !=PackageManager.PERMISSION_GRANTED)
                    requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.RECORD_AUDIO),STORAGE_REQUEST)
                else {

                    val intent = if(item.itemId== R.id.nav_record) Intent (MediaStore.Audio.Media.RECORD_SOUND_ACTION)
                    else
                        Intent(Intent.ACTION_PICK)



                    if (intent.resolveActivity(packageManager) != null) {
                        if(item.itemId==R.id.nav_gallery) {
                            intent.type="image/*"
                            startActivityForResult(intent, IMAGE_REQUEST)

                        }
                        else
                            if(item.itemId==R.id.nav_gallery_video) {
                                intent.type="video/*"
                                startActivityForResult(intent, VIDEO_RQUEST)
                            }
                        else{

                                startActivityForResult(intent, RECORD_REQUEST)}
                    }
                }
            }
            R.id.nav_schedule -> {

            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }



  fun onMove(joy:JoystickView ,angle: Int, strength: Int) {
                when(joy.getId())
                {
                    R.id.joystickLeft->{
                       // Log.d("joystick","joystickLeft")
                        if (joystickRight.normalizedX==50){
                            Log.d("Joystick","YJoystick"+joystickLeft.normalizedY)
                            var msg= ("Y:"+ joystickLeft.normalizedY.toString()).toByteArray()

                            netManager?.sendPackage(msg)
                        }
                    }
                    R.id.joystickRight ->{
                       // Log.d("joystick","joystickRight")
                        if(joystickLeft.normalizedY==50)
                        {
                            Log.d("Joystick","XJoystick:"+joystickRight.normalizedX)
                            var msg= ("X:"+ joystickRight.normalizedX.toString()).toByteArray()

                            netManager?.sendPackage(msg)
                        }
                    }
                }
    }
    fun createFixedNotification(){
        val intent=Intent(this,MainActivity::class.java)
        intent.setAction(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
        notificationManager=getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // netManager.getServerIp()
        notificationChannel= NotificationChannel(channelId,description,NotificationManager.IMPORTANCE_HIGH)
        notificationManager.createNotificationChannel(notificationChannel)
        builder=Notification.Builder(this,channelId).setContentTitle("ProcarConexion").
            setSmallIcon(R.drawable.ic_car_icon).
            setContentIntent(pendingIntent).setOngoing(true)


        filenotificationManager=getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val pendingIntent2=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT)
        val notificationFileChannel=NotificationChannel(channelId,"FileStatus",NotificationManager.IMPORTANCE_DEFAULT)
        filenotificationManager .createNotificationChannel(notificationFileChannel)
        builder_filestatusnotification=Notification.Builder(this,channelId).setContentTitle("TransmisionStatus").setSmallIcon(R.drawable.ic_car_icon)
            .setContentIntent(pendingIntent2)
       // notificationManager.notify(notificationId,builder.build())

         barbuilder = Notification.Builder(this, channelId).
            setContentTitle("File Transfer").
            setContentText("Transfering").
            setSmallIcon(R.drawable.ic_refresh).
            setPriority(NotificationCompat.PRIORITY_DEFAULT).setOnlyAlertOnce(true)




    }

     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
         Log.d("result",requestCode.toString()+" ok? "+resultCode.toString())
         if(resultCode== Activity.RESULT_CANCELED)
         {
             val noFile=File(currentMediaPath)
             if(noFile.exists())
             noFile.delete()
             return

         }

         if(resultCode!=Activity.RESULT_OK)
             return
         when (requestCode)
         {   CAMERA_PHOTO_REQUEST,CAMERA_VIDEO_REQUEST->{
             Log.d("Camera",currentMediaPath)
             val file=File(currentMediaPath)
             var filnm=file.name

             currFileSize=file.length()
             var input=FileInputStream(file)
             if(requestCode==CAMERA_PHOTO_REQUEST)
                netManager?.setbuffer(input,file.length(),"picture",filnm)
             else if(requestCode==CAMERA_VIDEO_REQUEST)
                 netManager?.setbuffer(input,file.length(),"video",filnm)

             Log.d("FileSize",file.length().toString())

         }

             IMAGE_REQUEST,VIDEO_RQUEST,RECORD_REQUEST->{
                 var currPFD:ParcelFileDescriptor?=null
                 //val ur=data?.data
                 //Log.d("Filename",getRealPathFromURI(this, ur!!))
               val selectedImage = data?.getData()
                 val filePathColumn = arrayOf( MediaStore.Files.FileColumns.DATA )

                val cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null)
                cursor?.moveToFirst()

                var columnIndex =0
                 cursor?.also {
                     columnIndex=try {
                         it.getColumnIndex(MediaStore.Files.FileColumns.DATA)
                     }catch (e:Exception) {
                         0

                     }
                 }

                    if(columnIndex <0)
                        columnIndex=0
                 try {
                     Log.d("file", columnIndex.toString())
                     val filenm = cursor.getString(0)
                     currentMediaPath = filenm
                     val file = File(currentMediaPath)
                     Log.d("file", filenm)


                     val input = FileInputStream(file)
                     if (requestCode == IMAGE_REQUEST)
                         netManager?.setbuffer(input, file.length(), "picture", file.name)
                     else if (requestCode == VIDEO_RQUEST)
                         netManager?.setbuffer(input, file.length(), "video", file.name)
                     else
                         netManager?.setbuffer(input, file.length(), "audio", file.name)
                 }catch (e:Exception)
                 {
                     builder_filestatusnotification.setContentText("Ups ese Archivo no lo puedo tomar..")
                     filenotificationManager.notify(notificationId+3,builder_filestatusnotification.build())

                 }
/*
                 data?.data?.also { uri ->
                     //Log.d("GetFileDataFromUri","name: "+getPath(uri))
                   //  var filen=File(uri.path)
                     //Log.d("GetFileDataFromUri","name: "+filen.name+" Filesize"+filen.length())

                     currPFD=try {
                         contentResolver.openFileDescriptor(uri,"r")
                     }catch (e:FileNotFoundException)
                     {
                         e.printStackTrace()
                         Log.e("GalleryFile", "File not found.")
                         return
                     }
                     val fd=currPFD?.fileDescriptor
                     var input=FileInputStream(fd)
                     val mediacolumns= arrayOf(MediaStore.Files.FileColumns.SIZE,MediaStore.Files.FileColumns.DISPLAY_NAME,MediaStore.Files.FileColumns.DATA)
                     var cursor=contentResolver.query(uri,mediacolumns,null,null,null)

                     cursor?.also {
                         it.moveToFirst()
                         var sizecol=it.getColumnIndex(mediacolumns[0])
                         var filesize=cursor.getLong(sizecol)
                         var filenmcol=it.getColumnIndex(mediacolumns[1])
                         var filenm=it.getString(filenmcol)
                         var filePathcol=it.getColumnIndex(mediacolumns[2])
                         val filept=it.getString(filePathcol)


                       //  Log.d("filename",filept)


                         if(requestCode==IMAGE_REQUEST)
                             netManager?.setbuffer(input,filesize,"picture",filenm)
                         else
                             netManager?.setbuffer(input,filesize,"video",filenm)


                     }

                     //Log.d("FILEDES",file.exists())

                 }
*/

             }

         }



    }


    @Throws(IOException::class)
private fun createFile(reqType:Int) : File {
    var strtype=""
    var suffix=""
    var prefix=""
    val timeStamp:String=SimpleDateFormat("yyyMMdd_HHmmss").format(Date())
        when(reqType){
        CAMERA_VIDEO_REQUEST->{  strtype="Video"
        prefix="${timeStamp}_"
            suffix=".mp4"
        }

        CAMERA_PHOTO_REQUEST->{strtype="Pictures"
        prefix="${timeStamp}_"
            suffix=".jpg"

        }


    }


    val storageDir:File= File(Environment.getExternalStorageDirectory().absolutePath+"/"+"Procar/"+strtype)
    if(!storageDir.exists())
        storageDir.mkdirs()
    return File.createTempFile(prefix,suffix,storageDir).apply {
        currentMediaPath=absolutePath
    }

}

    override fun onDestroy() {
        super.onDestroy()
        notificationManager.cancelAll()
        netManager?.closeAll()
        android.os.Process.killProcess(android.os.Process.myPid())

    }



}

